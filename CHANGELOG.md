# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project
adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.0.0] - 2020-04-14

### Changed

- Support ESLint 7.

### Fixed

- Fix infinite recursion when `ESLINT_CODE_QUALITY_REPORT` refers to `eslint-formatter-gitlab`
  itself.

## [1.1.0] - 2019-08-08

### Changed

- Support ESLint 6.

## [1.0.2] - 2018-12-13

### Fixes

- Fix automated release process.

## [1.0.1] - 2018-12-13

### Added

- Tests.
- Link to example merge request.

## [1.0.0] - 2018-11-29

### Added

- Initial release.
